﻿namespace ReportDBFNSC09
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCabang = new System.Windows.Forms.TextBox();
            this.btnBrowse09 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnsc09 = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnYearly = new System.Windows.Forms.Button();
            this.btnMonthly = new System.Windows.Forms.Button();
            this.btnDaily = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtCabang);
            this.groupBox1.Controls.Add(this.btnBrowse09);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtnsc09);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 84);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "CABANG";
            // 
            // txtCabang
            // 
            this.txtCabang.Location = new System.Drawing.Point(75, 19);
            this.txtCabang.Name = "txtCabang";
            this.txtCabang.Size = new System.Drawing.Size(166, 20);
            this.txtCabang.TabIndex = 10;
            // 
            // btnBrowse09
            // 
            this.btnBrowse09.Location = new System.Drawing.Point(424, 43);
            this.btnBrowse09.Name = "btnBrowse09";
            this.btnBrowse09.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse09.TabIndex = 8;
            this.btnBrowse09.Text = "Browse";
            this.btnBrowse09.UseVisualStyleBackColor = true;
            this.btnBrowse09.Click += new System.EventHandler(this.btnBrowse09_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "NSC09";
            // 
            // txtnsc09
            // 
            this.txtnsc09.Location = new System.Drawing.Point(75, 45);
            this.txtnsc09.Name = "txtnsc09";
            this.txtnsc09.Size = new System.Drawing.Size(343, 20);
            this.txtnsc09.TabIndex = 5;
            this.txtnsc09.WordWrap = false;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(421, 102);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(103, 23);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnYearly
            // 
            this.btnYearly.Location = new System.Drawing.Point(312, 102);
            this.btnYearly.Name = "btnYearly";
            this.btnYearly.Size = new System.Drawing.Size(103, 23);
            this.btnYearly.TabIndex = 14;
            this.btnYearly.Text = "Yearly Report";
            this.btnYearly.UseVisualStyleBackColor = true;
            this.btnYearly.Click += new System.EventHandler(this.btnYearly_Click);
            // 
            // btnMonthly
            // 
            this.btnMonthly.Location = new System.Drawing.Point(203, 102);
            this.btnMonthly.Name = "btnMonthly";
            this.btnMonthly.Size = new System.Drawing.Size(103, 23);
            this.btnMonthly.TabIndex = 15;
            this.btnMonthly.Text = "Monthly Report";
            this.btnMonthly.UseVisualStyleBackColor = true;
            this.btnMonthly.Click += new System.EventHandler(this.btnMonthly_Click);
            // 
            // btnDaily
            // 
            this.btnDaily.Location = new System.Drawing.Point(94, 102);
            this.btnDaily.Name = "btnDaily";
            this.btnDaily.Size = new System.Drawing.Size(103, 23);
            this.btnDaily.TabIndex = 16;
            this.btnDaily.Text = "Daily Report";
            this.btnDaily.UseVisualStyleBackColor = true;
            this.btnDaily.Click += new System.EventHandler(this.btnDaily_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 133);
            this.Controls.Add(this.btnDaily);
            this.Controls.Add(this.btnMonthly);
            this.Controls.Add(this.btnYearly);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "REKAP";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCabang;
        private System.Windows.Forms.Button btnBrowse09;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnsc09;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnYearly;
        private System.Windows.Forms.Button btnMonthly;
        private System.Windows.Forms.Button btnDaily;
    }
}

