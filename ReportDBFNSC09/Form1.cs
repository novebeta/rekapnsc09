﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;
using OfficeOpenXml;

namespace ReportDBFNSC09
{
    public partial class Form1 : Form
    {
        private const string SqLite = "rekap.db";
        private readonly SQLiteConnection con;
        private SQLiteDataAdapter adapter;
        private SQLiteCommand cmd;

        public Form1()
        {
            InitializeComponent();
            con = new SQLiteConnection(string.Format("Data Source={0};Compress=True;", SqLite));
        }

        private void btnBrowse09_Click(object sender, EventArgs e)
        {
            var fileOpen = new OpenFileDialog();
            fileOpen.CheckFileExists = true;
            fileOpen.Filter = "DBF Files|*.DBF";
            DialogResult rsl = fileOpen.ShowDialog();
            txtnsc09.Text = fileOpen.FileName;
            if (txtnsc09.Text.Length == 0)
            {
                return;
            }
            DataTable dt9;
            try
            {
                con.Open();
                dt9 = ParseDBF.ReadDBF(txtnsc09.Text);
                using (DbTransaction dbTrans = con.BeginTransaction())
                {
                    using (DbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "DROP TABLE IF EXISTS rekap;";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = @"CREATE TABLE rekap (
                        TANGGAL  TEXT(10),
                        NOFAK  TEXT(8),
                        DOKTER  TEXT(10),
                        KODEBRG  TEXT(10),
                        JASA  INTEGER,
                        JSDR  INTEGER,
                        QTY  REAL(10,2),
                        SATUAN  TEXT(3),
                        HARGA  REAL(10,2),
                        DISCOUNT  REAL(10,2)
                        );";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText =
                            "INSERT INTO rekap(TANGGAL, NOFAK, DOKTER, KODEBRG, JASA, JSDR, QTY, SATUAN, HARGA, DISCOUNT) VALUES(?,?,?,?,?,?,?,?,?,?)";
                        DbParameter Field1 = cmd.CreateParameter();
                        DbParameter Field2 = cmd.CreateParameter();
                        DbParameter Field3 = cmd.CreateParameter();
                        DbParameter Field4 = cmd.CreateParameter();
                        DbParameter Field5 = cmd.CreateParameter();
                        DbParameter Field6 = cmd.CreateParameter();
                        DbParameter Field7 = cmd.CreateParameter();
                        DbParameter Field8 = cmd.CreateParameter();
                        DbParameter Field9 = cmd.CreateParameter();
                        DbParameter Field10 = cmd.CreateParameter();
                        cmd.Parameters.Add(Field1);
                        cmd.Parameters.Add(Field2);
                        cmd.Parameters.Add(Field3);
                        cmd.Parameters.Add(Field4);
                        cmd.Parameters.Add(Field5);
                        cmd.Parameters.Add(Field6);
                        cmd.Parameters.Add(Field7);
                        cmd.Parameters.Add(Field8);
                        cmd.Parameters.Add(Field9);
                        cmd.Parameters.Add(Field10);
                        foreach (DataRow dataRow in dt9.Rows)
                        {
                            var tgl = dataRow.Field<DateTime?>("TANGGAL");
                            Field1.Value = (!tgl.HasValue) ? null : tgl.Value.ToString("yyyy-MM-dd");
                            Field2.Value = dataRow.Field<string>("NOFAK").Trim().ToUpper();
                            Field3.Value = dataRow.Field<string>("DOKTER").Trim().ToUpper();
                            Field4.Value = dataRow.Field<string>("KODEBRG").Trim().ToUpper();
                            Field5.Value = dataRow.Field<int>("JASA");
                            Field6.Value = dataRow.Field<int>("JSDR");
                            Field7.Value = dataRow.Field<decimal>("QTY");
                            Field8.Value = dataRow.Field<string>("SATUAN").Trim().ToUpper();
                            Field9.Value = dataRow.Field<decimal>("HARGA");
                            Field10.Value = dataRow.Field<int>("DISCOUNT");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    dbTrans.Commit();
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Anda yakin akan keluar?",
                "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Close();
            }
        }

        private void btnDaily_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                using (DbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"SELECT r.TANGGAL,r.KODEBRG,ifnull(Sum(r.QTY),0) TOTAL
                    FROM rekap AS r
                    WHERE r.SATUAN <> 'ORG'
                    GROUP BY r.TANGGAL,r.KODEBRG";
                    DbDataReader reader = cmd.ExecuteReader();
                    using (var p = new ExcelPackage())
                    {
                        int rowIndex = 1;
                        p.Workbook.Worksheets.Add("Daily Report");
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        ws.Name = "Daily Report";
                        ws.Cells[rowIndex, 1].Value = "CABANG";
                        ws.Cells[rowIndex, 2].Value = txtCabang.Text.ToUpper();
                        rowIndex++;
                        ws.Cells[rowIndex, 1].Value = "TANGGAL";
                        ws.Cells[rowIndex, 2].Value = "KODEBRG";
                        ws.Cells[rowIndex, 3].Value = "TOTAL";
                        rowIndex++;
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ws.Cells[rowIndex, 1].Value = reader.GetString(0);
                                ws.Cells[rowIndex, 2].Value = reader.GetString(1);
                                ws.Cells[rowIndex, 3].Value = reader.GetDecimal(2);
                                rowIndex++;
                            }
                        }
                        reader.Close();
                        Byte[] bin = p.GetAsByteArray();
                        File.WriteAllBytes(@"D:\"+txtCabang.Text.ToUpper()+"-ReportDaily.xlsx", bin);
                    }
                }
                MessageBox.Show("Generate Report Harian Selesai");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void btnMonthly_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                using (DbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"SELECT r.KODEBRG,strftime('%Y', r.TANGGAL) TAHUN,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '01' THEN r.QTY ELSE 0 END),0) JAN,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '02' THEN r.QTY ELSE 0 END),0) FEB,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '03' THEN r.QTY ELSE 0 END),0) MAR,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '04' THEN r.QTY ELSE 0 END),0) APR,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '05' THEN r.QTY ELSE 0 END),0) MEI,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '06' THEN r.QTY ELSE 0 END),0) JUN,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '07' THEN r.QTY ELSE 0 END),0) JUL,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '08' THEN r.QTY ELSE 0 END),0) AUG,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '09' THEN r.QTY ELSE 0 END),0) SEPT,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '10' THEN r.QTY ELSE 0 END),0) OKT,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '11' THEN r.QTY ELSE 0 END),0) NOP,
ifnull(sum(CASE strftime('%m', r.TANGGAL) WHEN '12' THEN r.QTY ELSE 0 END),0) DEC,
ifnull(sum(r.QTY),0) TOTAL
FROM rekap AS r WHERE r.SATUAN <> 'ORG'
GROUP BY r.KODEBRG,TAHUN
ORDER BY TAHUN,r.KODEBRG;";
                    DbDataReader reader = cmd.ExecuteReader();
                    using (var p = new ExcelPackage())
                    {
                        int rowIndex = 1;
                        p.Workbook.Worksheets.Add("Monthly Report");
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        ws.Name = "Monthly Report";
                        ws.Cells[rowIndex, 1].Value = "CABANG";
                        ws.Cells[rowIndex, 2].Value = txtCabang.Text.ToUpper();
                        rowIndex++;
                        ws.Cells[rowIndex, 1].Value = "KODEBRG";
                        ws.Cells[rowIndex, 2].Value = "TAHUN";
                        ws.Cells[rowIndex, 3].Value = "JAN";
                        ws.Cells[rowIndex, 4].Value = "FEB";
                        ws.Cells[rowIndex, 5].Value = "MAR";
                        ws.Cells[rowIndex, 6].Value = "APR";
                        ws.Cells[rowIndex, 7].Value = "MEI";
                        ws.Cells[rowIndex, 8].Value = "JUN";
                        ws.Cells[rowIndex, 9].Value = "JUL";
                        ws.Cells[rowIndex, 10].Value = "AUG";
                        ws.Cells[rowIndex, 11].Value = "SEPT";
                        ws.Cells[rowIndex, 12].Value = "OKT";
                        ws.Cells[rowIndex, 13].Value = "NOP";
                        ws.Cells[rowIndex, 14].Value = "DEC";
                        ws.Cells[rowIndex, 15].Value = "TOTAL";
                        rowIndex++;
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ws.Cells[rowIndex, 1].Value = reader.GetString(0);
                                ws.Cells[rowIndex, 2].Value = reader.GetString(1);
                                ws.Cells[rowIndex, 3].Value = reader.GetDecimal(2);
                                ws.Cells[rowIndex, 4].Value = reader.GetDecimal(3);
                                ws.Cells[rowIndex, 5].Value = reader.GetDecimal(4);
                                ws.Cells[rowIndex, 6].Value = reader.GetDecimal(5);
                                ws.Cells[rowIndex, 7].Value = reader.GetDecimal(6);
                                ws.Cells[rowIndex, 8].Value = reader.GetDecimal(7);
                                ws.Cells[rowIndex, 9].Value = reader.GetDecimal(8);
                                ws.Cells[rowIndex, 10].Value = reader.GetDecimal(9);
                                ws.Cells[rowIndex, 11].Value = reader.GetDecimal(10);
                                ws.Cells[rowIndex, 12].Value = reader.GetDecimal(11);
                                ws.Cells[rowIndex, 13].Value = reader.GetDecimal(12);
                                ws.Cells[rowIndex, 14].Value = reader.GetDecimal(13);
                                ws.Cells[rowIndex, 15].Value = reader.GetDecimal(14);
                                rowIndex++;
                            }
                        }
                        reader.Close();
                        Byte[] bin = p.GetAsByteArray();
                        File.WriteAllBytes(@"D:\" + txtCabang.Text.ToUpper() + "-ReportMonthly.xlsx", bin);
                    }
                }
                MessageBox.Show("Generate Report Bulanan Selesai");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void btnYearly_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                using (DbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"SELECT min(date(r.TANGGAL)) AWAL,max(date(r.TANGGAL)) AKHIR,
                    strftime('%Y', r.TANGGAL) TAHUN,r.KODEBRG,ifnull(sum(r.QTY),0) TOTAL
                    FROM rekap AS r
                    WHERE r.SATUAN <> 'ORG'
                    GROUP BY r.KODEBRG,TAHUN
                    ORDER BY date(r.TANGGAL)";
                    DbDataReader reader = cmd.ExecuteReader();
                    using (var p = new ExcelPackage())
                    {
                        int rowIndex = 1;
                        p.Workbook.Worksheets.Add("Yearly Report");
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        ws.Name = "Yearly Report";
                        ws.Cells[rowIndex, 1].Value = "CABANG";
                        ws.Cells[rowIndex, 2].Value = txtCabang.Text.ToUpper();
                        rowIndex++;
                        ws.Cells[rowIndex, 1].Value = "AWAL";
                        ws.Cells[rowIndex, 2].Value = "AKHIR";
                        ws.Cells[rowIndex, 3].Value = "TAHUN";
                        ws.Cells[rowIndex, 4].Value = "KODEBRG";
                        ws.Cells[rowIndex, 5].Value = "TOTAL";
                        rowIndex++;
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ws.Cells[rowIndex, 1].Value = reader.GetString(0);
                                ws.Cells[rowIndex, 2].Value = reader.GetString(1);
                                ws.Cells[rowIndex, 3].Value = reader.GetDecimal(2);
                                ws.Cells[rowIndex, 4].Value = reader.GetString(3);
                                ws.Cells[rowIndex, 5].Value = reader.GetDecimal(4);
                                rowIndex++;
                            }
                        }
                        reader.Close();
                        Byte[] bin = p.GetAsByteArray();
                        File.WriteAllBytes(@"D:\" + txtCabang.Text.ToUpper() + "-ReportYearly.xlsx", bin);
                    }
                }
                MessageBox.Show("Generate Report Tahunan Selesai");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
}